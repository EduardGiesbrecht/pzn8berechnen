﻿namespace pzn8Berechnen
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.berechnen_btn = new System.Windows.Forms.Button();
            this.pzn_txt = new System.Windows.Forms.TextBox();
            this.pzn_lbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // berechnen_btn
            // 
            this.berechnen_btn.Location = new System.Drawing.Point(90, 60);
            this.berechnen_btn.Name = "berechnen_btn";
            this.berechnen_btn.Size = new System.Drawing.Size(75, 23);
            this.berechnen_btn.TabIndex = 0;
            this.berechnen_btn.Text = "Berechnen";
            this.berechnen_btn.UseVisualStyleBackColor = true;
            this.berechnen_btn.Click += new System.EventHandler(this.berechnen_btn_Click);
            // 
            // pzn_txt
            // 
            this.pzn_txt.Location = new System.Drawing.Point(90, 116);
            this.pzn_txt.Name = "pzn_txt";
            this.pzn_txt.Size = new System.Drawing.Size(100, 20);
            this.pzn_txt.TabIndex = 1;
            // 
            // pzn_lbl
            // 
            this.pzn_lbl.AutoSize = true;
            this.pzn_lbl.Location = new System.Drawing.Point(120, 181);
            this.pzn_lbl.Name = "pzn_lbl";
            this.pzn_lbl.Size = new System.Drawing.Size(35, 13);
            this.pzn_lbl.TabIndex = 2;
            this.pzn_lbl.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.pzn_lbl);
            this.Controls.Add(this.pzn_txt);
            this.Controls.Add(this.berechnen_btn);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button berechnen_btn;
        private System.Windows.Forms.TextBox pzn_txt;
        private System.Windows.Forms.Label pzn_lbl;
    }
}

