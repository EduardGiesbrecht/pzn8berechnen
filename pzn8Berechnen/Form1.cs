﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pzn8Berechnen
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void berechnen_btn_Click(object sender, EventArgs e)
        {
            string pzn = calcPzn(pzn_txt.Text);
            pzn_lbl.Text = "Die Nummer inkl Prüfziffer am ende lautet:" + pzn;
        }

        private string calcPzn(string content)
        {
            string contentMitNullen  = content;

            //Mit 0 auffüllen
            while (contentMitNullen.Length < 7)
            {
                contentMitNullen = "0" + contentMitNullen;
            }

            //Summieren der Produkte
            int summieren = 0;

            for (int i = 0; i < contentMitNullen.Length; i++)
            {
                int gewichtung = i + 1;
                summieren += Convert.ToInt32(contentMitNullen[i].ToString()) * gewichtung;
            }

            //Ergebniss durch Modulo
            int ergModulo = summieren % 11;

            //Ergebniss Rest 10 abfangen
            if (ergModulo == 10)
            {
                return "Ungültige PZN";
            }

            return contentMitNullen + "/" + ergModulo.ToString();
        }
    }
}